package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectedPilotRescueController {

    @Autowired
    private InMemoryMapDataBase inMemoryMapDataBase;

    @Autowired
    private AirplanesAllocationManager airplanesAllocationManager;

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getAllEjections() {
            return inMemoryMapDataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("/takeResponsibility")
    public void updateStatusInjection(@RequestParam int ejectionId, @CookieValue(value = "client-id", defaultValue = "noId") String clientId) {
        EjectedPilotInfo ejectedPilotToUpdate = inMemoryMapDataBase.getByID(ejectionId, EjectedPilotInfo.class);
        if(ejectedPilotToUpdate.getRescuedBy() == null) {
            ejectedPilotToUpdate.setRescuedBy(clientId);
            inMemoryMapDataBase.update(ejectedPilotToUpdate);
            airplanesAllocationManager.allocateClosestAirplanesForEjection(ejectedPilotToUpdate, clientId);
        }
    }
}
