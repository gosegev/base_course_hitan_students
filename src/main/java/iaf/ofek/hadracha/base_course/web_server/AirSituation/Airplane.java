package iaf.ofek.hadracha.base_course.web_server.AirSituation;

import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Airplane implements Entity<Airplane> {
    private AirplaneKind airplaneKind;

    private int id;

    private Coordinates coordinates;
    private Coordinates headingTo;
    public String controllerClientId;

    /**
     * Azimuth in degrees. 0 indicates north, 90 indicates east
     */
    private double azimuth;
    /**
     * Velocity in KPH.
     */
    private double velocity;

    /**
     * The acceleration of change of azimuth
     */
    private double radialAcceleration;

    private List<Consumer<Airplane>> arrivedAtDestinationCallbacks = new ArrayList<>(1);


    public Airplane(AirplaneKind airplaneKind, int id) {
        this.airplaneKind = airplaneKind;
        this.id = id;
    }

    public AirplaneKind getAirplaneKind() {
        return airplaneKind;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Azimuth in degrees. 0 indicates north, 90 indicates east
     */
    public double getAzimuth() {
        return azimuth;
    }

    /**
     * Sets the azimuth of the aircraft. The given azimuth will be normalized to [0..360]
     */
    public void setAzimuth(double azimuth) {
        while (azimuth < 0)
            azimuth += 360;
        this.azimuth = azimuth % 360;
    }

    @Override
    public Airplane clone() {
        try {
            return (Airplane) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError("Not possible - Entity implements Cloneable");
        }
    }

    public void flyTo(Coordinates headTo, String controllerClientId) {
        headingTo = headTo;
        this.controllerClientId = controllerClientId;
    }

    public boolean isAllocated() {
        return this.controllerClientId != null;
    }

    public void unAllocate() {
        this.headingTo = null;
        this.controllerClientId = null;
        arrivedAtDestinationCallbacks.clear();
    }

    public void onArrivedAtDestination(Consumer<Airplane> callback) {
        arrivedAtDestinationCallbacks.add(callback);
    }

    void raiseArrivedAtDestinationEvent() {
        //why new array list? because callback might call unAllocate which modifies this collection
        new ArrayList<>(arrivedAtDestinationCallbacks).forEach(airplaneConsumer -> airplaneConsumer.accept(this));
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }

    public Coordinates getHeadingTo() {
        return this.headingTo;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public double getVelocity() {
        return this.velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    public double getRadialAcceleration(){
        return this.radialAcceleration;
    }

    public void setRadialAcceleration(double radialAcceleration){
        this.radialAcceleration = radialAcceleration;
    }
}
